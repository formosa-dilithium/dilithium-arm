/*************************************************
* Name:        JASMIN_MLDSA_polyeta_pack
*
* Description: Bit-pack polynomial with coefficients in [-ETA,ETA].
*
* Arguments:   - uint8_t *r: pointer to output byte array with at least
*                            POLYETA_PACKEDBYTES bytes
*              - const poly *a: pointer to input polynomial
**************************************************/

inline fn __polyeta_pack(reg ptr u8[POLYETA_PACKEDBYTES] r, reg ptr u32[N] a) ->
       reg ptr u8[POLYETA_PACKEDBYTES] {
  reg u32 i, j;
  reg u32[8] t;
  /*
  for (i = 0; i < N / 8; ++i) {
      t[0] = (uint8_t) (ETA - a->coeffs[8 * i + 0]);
      t[1] = (uint8_t) (ETA - a->coeffs[8 * i + 1]);
      t[2] = (uint8_t) (ETA - a->coeffs[8 * i + 2]);
      t[3] = (uint8_t) (ETA - a->coeffs[8 * i + 3]);
      t[4] = (uint8_t) (ETA - a->coeffs[8 * i + 4]);
      t[5] = (uint8_t) (ETA - a->coeffs[8 * i + 5]);
      t[6] = (uint8_t) (ETA - a->coeffs[8 * i + 6]);
      t[7] = (uint8_t) (ETA - a->coeffs[8 * i + 7]);

      r[3 * i + 0]  = (t[0] >> 0) | (t[1] << 3) | (t[2] << 6);
      r[3 * i + 1]  = (t[2] >> 2) | (t[3] << 1) | (t[4] << 4) | (t[5] << 7);
      r[3 * i + 2]  = (t[5] >> 1) | (t[6] << 2) | (t[7] << 5);
  }
  */

  i = 0; j = 0;
  while(i < N) {
    inline int k;
    for k = 0 to 8 {
      t[k] = __eta_minus_coeff(a, i);
      i += 1;
    }
    reg u32 aux;
    aux = __or_lsl(t[0], t[1], 3);
    aux = __or_lsl_inplace(aux, t[2], 6);
    r[(int) j] = aux; j += 1;
    aux = __ubfx(t[2], 2, 6);
    aux = __or_lsl_inplace(aux, t[3], 1);
    aux = __or_lsl_inplace(aux, t[4], 4);
    aux = __or_lsl_inplace(aux, t[5], 7);
    r[(int) j] = aux;
    j += 1;
    aux = __ubfx(t[5], 1, 7);
    aux = __or_lsl_inplace(aux, t[6], 2);
    aux = __or_lsl_inplace(aux, t[7], 5);
    r[(int) j] = aux; j += 1;
  }

  return r;
}

fn _polyeta_pack(reg ptr u8[POLYETA_PACKEDBYTES] r, reg ptr u32[N] a) ->
       reg ptr u8[POLYETA_PACKEDBYTES] {
   r = __polyeta_pack(r, a);
   return r;
}

/*************************************************
* Name:        JASMIN_MLDSA_polyeta_unpack
*
* Description: Unpack polynomial with coefficients in [-ETA,ETA].
*
* Arguments:   - poly *r: pointer to output polynomial
*              - const uint8_t *a: byte array with bit-packed polynomial
**************************************************/

inline fn __polyeta_unpack(reg ptr u32[N] r, reg ptr u8[POLYETA_PACKEDBYTES] a) -> reg ptr u32[N] {
  reg u32 i, j;
  reg u32[3] t;
  inline int k;
  reg u32 aux;
  i = 0; j = 0;
  while(i < N) {

    for k = 0 to 3 { t[k] = (32u) a[(int) j]; j +=1; }

    /* r->coeffs[8 * i + 0] =  (a[3 * i + 0] >> 0) & 7;
       r->coeffs[8 * i + 0] = ETA - r->coeffs[8 * i + 0]; */
    aux = __ubfx(t[0], 0, 3);
    aux = __imm_sub_reg_inplace(ETA, aux);
    r[(int) i] = aux; i += 1;

    /* r->coeffs[8 * i + 1] =  (a[3 * i + 0] >> 3) & 7;
       r->coeffs[8 * i + 1] = ETA - r->coeffs[8 * i + 1]; */
    aux = __ubfx(t[0], 3, 3);
    aux = __imm_sub_reg_inplace(ETA, aux);
    r[(int) i] = aux; i += 1;

    /* r->coeffs[8 * i + 2] = ((a[3 * i + 0] >> 6) | (a[3 * i + 1] << 2)) & 7;
       r->coeffs[8 * i + 2] = ETA - r->coeffs[8 * i + 2]; */
    aux = t[0] >> 6;
    aux = __or_lsl(aux, t[1], 2);
    aux &= 7;
    aux = __imm_sub_reg_inplace(ETA, aux);
    r[(int) i] = aux; i += 1;

    /* r->coeffs[8 * i + 3] =  (a[3 * i + 1] >> 1) & 7;
       r->coeffs[8 * i + 3] = eta - r->coeffs[8 * i + 3]; */
    aux = __ubfx(t[1], 1, 3);
    aux = __imm_sub_reg_inplace(ETA, aux);
    r[(int) i] = aux; i += 1;

    /* r->coeffs[8 * i + 4] =  (a[3 * i + 1] >> 4) & 7;
       r->coeffs[8 * i + 4] = eta - r->coeffs[8 * i + 4]; */
    aux = __ubfx(t[1], 4, 3);
    aux = __imm_sub_reg_inplace(ETA, aux);
    r[(int) i] = aux; i += 1;

    /* r->coeffs[8 * i + 5] = ((a[3 * i + 1] >> 7) | (a[3 * i + 2] << 1)) & 7;
       r->coeffs[8 * i + 5] = ETA - r->coeffs[8 * i + 5]; */
    aux = t[1] >> 7;
    aux = __or_lsl(aux, t[2], 1);
    aux &= 7;
    aux = __imm_sub_reg_inplace(ETA, aux);
    r[(int) i] = aux; i += 1;

    /* r->coeffs[8 * i + 6] =  (a[3 * i + 2] >> 2) & 7;
       r->coeffs[8 * i + 6] = eta - r->coeffs[8 * i + 6]; */
    aux = __ubfx(t[2], 2, 3);
    aux = __imm_sub_reg_inplace(ETA, aux);
    r[(int) i] = aux; i += 1;

    /* r->coeffs[8 * i + 7] =  (a[3 * i + 2] >> 5) & 7;
       r->coeffs[8 * i + 7] = eta - r->coeffs[8 * i + 7]; */
    aux = __ubfx(t[2], 5, 3);
    aux = __imm_sub_reg_inplace(ETA, aux);
    r[(int) i] = aux; i += 1;
  }

  return r;  
}

fn _polyeta_unpack(reg ptr u32[N] r, reg ptr u8[POLYETA_PACKEDBYTES] a) -> reg ptr u32[N] {
  r = __polyeta_unpack(r, a);
  return r;
}

/*************************************************
* Name:        JASMIN_MLDSA_polyt0_pack
*
* Description: Bit-pack polynomial t0 with coefficients in ]-2^{D-1}, 2^{D-1}].
*
* Arguments:   - uint8_t *r: pointer to output byte array with at least
*                            POLYT0_PACKEDBYTES bytes
*              - const poly *a: pointer to input polynomial
**************************************************/
inline fn __polyt0_pack(reg ptr u8[POLYT0_PACKEDBYTES] r, reg ptr u32[N] a) -> reg ptr u8[POLYT0_PACKEDBYTES] {
  reg u32 i, j, aux;
  reg u32[8] t;
  inline int k;
    
  i = 0; j = 0;
  while(i < N)
  {
    for k = 0 to 8 {
      t[k] = a[(int) i]; i += 1;
      t[k] = __imm_sub_reg_inplace((1 << (D - 1)), t[k]);
    }	
    /* r[13 * i + 0]  =  (uint8_t) t[0];           */
    r[(int) j] = t[0]; j += 1;   // j = 1
    
    /* r[13 * i + 1]  =  (uint8_t) (t[0] >>  8);   */
    /* r[13 * i + 1] |=  (uint8_t) (t[1] <<  5);   */
    aux = t[0] >> 8;
    aux = __or_lsl_inplace(aux, t[1], 5);
    r[(int) j] = aux; j += 1;  // j = 2
    
    /* r[13 * i + 2]  =  (uint8_t) (t[1] >>  3);   */
    aux = t[1] >> 3;
    r[(int) j] = aux; j += 1;   // j = 3
    
    /* r[13 * i + 3]  =  (uint8_t) (t[1] >> 11);   */
    /* r[13 * i + 3] |=  (uint8_t) (t[2] <<  2);   */
    aux = t[1] >> 11;
    aux = __or_lsl_inplace(aux, t[2], 2);
    r[(int) j] = aux; j += 1;  // j = 4

    /* r[13 * i + 4]  =  (uint8_t) (t[2] >>  6);   */
    /* r[13 * i + 4] |=  (uint8_t) (t[3] <<  7);   */
    aux = t[2] >> 6;
    aux = __or_lsl_inplace(aux, t[3], 7);
    r[(int) j] = aux; j += 1; // j = 5
    
    /* r[13 * i + 5]  =  (uint8_t) (t[3] >>  1);   */
    aux = t[3] >> 1;
    r[(int) j] = aux; j += 1; // j = 6
    
    /* r[13 * i + 6]  =  (uint8_t) (t[3] >>  9);   */
    /* r[13 * i + 6] |=  (uint8_t) (t[4] <<  4);   */
    aux = t[3] >> 9;
    aux = __or_lsl_inplace(aux, t[4], 4);
    r[(int) j] = aux; j += 1; // j = 7
    
    /* r[13 * i + 7]  =  (uint8_t) (t[4] >>  4);   */
    aux = t[4] >> 4;
    r[(int) j] = aux; j += 1;  // j = 8
    
    /* r[13 * i + 8]  =  (uint8_t) (t[4] >> 12);   */
    /* r[13 * i + 8] |=  (uint8_t) (t[5] <<  1);   */
    aux = t[4] >> 12;
    aux = __or_lsl_inplace(aux, t[5], 1);
    r[(int) j] = aux; j += 1;  // j = 9
    
    /* r[13 * i + 9]  =  (uint8_t) (t[5] >>  7);   */
    /* r[13 * i + 9] |=  (uint8_t) (t[6] <<  6);   */
    aux = t[5] >> 7;
    aux = __or_lsl_inplace(aux, t[6], 6);
    r[(int) j] = aux; j += 1;  // j = 10
    
    /* r[13 * i + 10]  =  (uint8_t) (t[6] >>  2);  */
    aux = t[6] >> 2;
    r[(int) j] = aux; j += 1;  // j = 11
    
    /* r[13 * i + 11]  =  (uint8_t) (t[6] >> 10);  */
    /* r[13 * i + 11] |=  (uint8_t) (t[7] <<  3);  */
    aux = t[6] >> 10;
    aux = __or_lsl_inplace(aux, t[7], 3);
    r[(int) j] = aux; j += 1; // j = 12
    
    /* r[13 * i + 12]  =  (uint8_t) (t[7] >>  5);  */
    aux = t[7] >> 5;
    r[(int)j] = aux; j += 1;  // j = 13
  }
  return r;
}

fn _polyt0_pack(reg ptr u8[POLYT0_PACKEDBYTES] r, reg ptr u32[N] a) -> reg ptr u8[POLYT0_PACKEDBYTES] {
  r = __polyt0_pack(r, a);
  return r;
}

/*************************************************
* Name:        JASMIN_MLDSA_polyt0_unpack
*
* Description: Unpack polynomial t0 with coefficients in ]-2^{D-1}, 2^{D-1}].
*
* Arguments:   - poly *r: pointer to output polynomial
*              - const uint8_t *a: byte array with bit-packed polynomial
**************************************************/
inline fn __polyt0_unpack(reg ptr u32[N] r, reg ptr u8[POLYT0_PACKEDBYTES] a) -> reg ptr u32[N] {
  reg u32 i, j, a_;
  reg u32[8] t;
  
  i = 0; j = 0;
  while(i < N) {
    /*  r->coeffs[8 * i + 0]  = a[13 * i + 0];
        r->coeffs[8 * i + 0] |= (uint32_t)a[13 * i + 1] << 8;
        r->coeffs[8 * i + 0] &= 0x1FFF; */
    t[0] = (32u) a[(int) j]; j += 1;  // j = 1
    a_ = (32u) a[(int) j]; j += 1;    // j = 2  a_ = a[1]

    t[0] = __or_lsl_inplace(t[0], a_, 8);
    t[0] = __ubfx(t[0], 0, 13);
    t[0] = __imm_sub_reg_inplace((1 << (D - 1)), t[0]);
    r[(int) i] = t[0]; i += 1;
	
    /*  r->coeffs[8 * i + 1]  = a[13 * i + 1] >> 5;
        r->coeffs[8 * i + 1] |= (uint32_t)a[13 * i + 2] << 3;
        r->coeffs[8 * i + 1] |= (uint32_t)a[13 * i + 3] << 11;
        r->coeffs[8 * i + 1] &= 0x1FFF; */
    t[1] = a_ >> 5;
    a_ = (32u) a[(int) j]; j += 1; // j = 3
    t[1] = __or_lsl_inplace(t[1], a_, 3);
    a_ = (32u) a[(int) j]; j += 1; // j = 4 a_ = a[3]
    t[1] = __or_lsl_inplace(t[1], a_, 11);
    t[1] = __ubfx(t[1], 0, 13);
    t[1] = __imm_sub_reg_inplace((1 << (D - 1)), t[1]);
    r[(int) i] = t[1]; i += 1;
	
    /*  r->coeffs[8 * i + 2]  = a[13 * i + 3] >> 2;
        r->coeffs[8 * i + 2] |= (uint32_t)a[13 * i + 4] << 6;
        r->coeffs[8 * i + 2] &= 0x1FFF; */
    t[2] = a_ >> 2;
    a_ = (32u) a[(int) j]; j += 1;  // j = 5  a_ = a[4]
    t[2] = __or_lsl_inplace(t[2], a_, 6);
    t[2] = __ubfx(t[2], 0, 13);

    t[2] = __imm_sub_reg_inplace((1 << (D - 1)), t[2]);
    r[(int) i] = t[2]; i += 1;
	    
    /*  r->coeffs[8 * i + 3]  = a[13 * i + 4] >> 7;
        r->coeffs[8 * i + 3] |= (uint32_t)a[13 * i + 5] << 1;
        r->coeffs[8 * i + 3] |= (uint32_t)a[13 * i + 6] << 9;
        r->coeffs[8 * i + 3] &= 0x1FFF; */
    t[3] = a_ >> 7;
    a_ = (32u) a[(int) j]; j += 1;  // j = 6
    t[3] = __or_lsl_inplace(t[3], a_, 1);
    a_ = (32u) a[(int) j]; j += 1; // j = 7 a_ = a[6]
    t[3] = __or_lsl_inplace(t[3], a_, 9);
    t[3] = __ubfx(t[3], 0, 13);
    t[3] = __imm_sub_reg_inplace((1 << (D - 1)), t[3]);
    r[(int) i] = t[3]; i += 1;
	
    /*  r->coeffs[8 * i + 4]  = a[13 * i + 6] >> 4;
        r->coeffs[8 * i + 4] |= (uint32_t)a[13 * i + 7] << 4;
        r->coeffs[8 * i + 4] |= (uint32_t)a[13 * i + 8] << 12;
        r->coeffs[8 * i + 4] &= 0x1FFF; */
    t[4] = a_ >> 4;
    a_ = (32u) a[(int) j]; j += 1;  // j = 8
    t[4] = __or_lsl_inplace(t[4], a_, 4);
    a_ = (32u) a[(int) j]; j += 1; // j = 9   a_ = a[8]
    t[4] = __or_lsl_inplace(t[4], a_, 12);
    t[4] = __ubfx(t[4], 0, 13);
    t[4] = __imm_sub_reg_inplace((1 << (D - 1)), t[4]);
    r[(int) i] = t[4]; i += 1;

    /*  r->coeffs[8 * i + 5]  = a[13 * i + 8] >> 1;
        r->coeffs[8 * i + 5] |= (uint32_t)a[13 * i + 9] << 7;
        r->coeffs[8 * i + 5] &= 0x1FFF; */
    t[5] = a_ >> 1;
    a_ = (32u) a[(int) j]; j += 1; // j = 10   a_ = a[9]
    t[5] = __or_lsl_inplace(t[5], a_, 7);
    t[5] = __ubfx(t[5], 0, 13);
    t[5] = __imm_sub_reg_inplace((1 << (D - 1)), t[5]);
    r[(int) i] = t[5]; i += 1;

    /*  r->coeffs[8 * i + 6]  = a[13 * i + 9] >> 6;
        r->coeffs[8 * i + 6] |= (uint32_t)a[13 * i + 10] << 2;
        r->coeffs[8 * i + 6] |= (uint32_t)a[13 * i + 11] << 10;
        r->coeffs[8 * i + 6] &= 0x1FFF; */
    t[6] = a_ >> 6;
    a_ = (32u) a[(int) j]; j += 1; // j = 11   a_ = a[10]
    t[6] = __or_lsl_inplace(t[6], a_, 2);
    a_ = (32u) a[(int) j]; j += 1; // j = 12   a_ = a[11]
    t[6] = __or_lsl_inplace(t[6], a_, 10);
    t[6] = __ubfx(t[6], 0, 13);
    t[6] = __imm_sub_reg_inplace((1 << (D - 1)), t[6]);
    r[(int) i] = t[6]; i += 1;
	
    /*  r->coeffs[8 * i + 7]  = a[13 * i + 11] >> 3;
        r->coeffs[8 * i + 7] |= (uint32_t)a[13 * i + 12] << 5;
        r->coeffs[8 * i + 7] &= 0x1FFF; */
    t[7] = a_ >> 3;
    a_ = (32u) a[(int) j]; j += 1; // j = 13   a_ = a[12]
    t[7] = __or_lsl_inplace(t[7], a_, 5);
    t[7] = __ubfx(t[7], 0, 13);
    t[7] = __imm_sub_reg_inplace((1 << (D - 1)), t[7]);
    r[(int) i] = t[7]; i += 1;

    /* This is moved up */
    /*  r->coeffs[8 * i + 0] = (1 << (D - 1)) - r->coeffs[8 * i + 0];
        r->coeffs[8 * i + 1] = (1 << (D - 1)) - r->coeffs[8 * i + 1];
        r->coeffs[8 * i + 2] = (1 << (D - 1)) - r->coeffs[8 * i + 2];
        r->coeffs[8 * i + 3] = (1 << (D - 1)) - r->coeffs[8 * i + 3];
        r->coeffs[8 * i + 4] = (1 << (D - 1)) - r->coeffs[8 * i + 4];
        r->coeffs[8 * i + 5] = (1 << (D - 1)) - r->coeffs[8 * i + 5];
        r->coeffs[8 * i + 6] = (1 << (D - 1)) - r->coeffs[8 * i + 6];
        r->coeffs[8 * i + 7] = (1 << (D - 1)) - r->coeffs[8 * i + 7]; */
  }
  return r;
}

fn _polyt0_unpack(reg ptr u32[N] r, reg ptr u8[POLYT0_PACKEDBYTES] a) -> reg ptr u32[N] {
  r = __polyt0_unpack(r, a);
  return r;
}  

/*************************************************
* Name:        JASMIN_MLDSA_polyt1_pack
*
* Description: Bit-pack polynomial t1 with coefficients fitting in 10 bits.
*              Input coefficients are assumed to be standard representatives.
*
* Arguments:   - uint8_t *r: pointer to output byte array with at least
*                            POLYT1_PACKEDBYTES bytes
*              - const poly *a: pointer to input polynomial
**************************************************/
inline fn __polyt1_pack(reg ptr u8[POLYT1_PACKEDBYTES] r, reg ptr u32[N] a) -> reg ptr u8[POLYT1_PACKEDBYTES] {
  reg u32 i, j, aux;
  reg u32[4] t;
  inline int k;
  i = 0; j = 0;
  while(i < N)
  {
    for k = 0 to 4 {
      t[k] = a[(int) i]; i += 1;
    }
    /* r[5 * i + 0] = (uint8_t) (a->coeffs[4 * i + 0] >> 0); */
    r[(int) j] = t[0]; j += 1;
    
    /* r[5 * i + 1] = (uint8_t) ((a->coeffs[4 * i + 0] >> 8) | (a->coeffs[4 * i + 1] << 2)); */
    aux = t[0] >> 8;
    aux = __or_lsl_inplace(aux, t[1], 2);
    r[(int) j] = aux; j += 1;

    /* r[5 * i + 2] = (uint8_t) ((a->coeffs[4 * i + 1] >> 6) | (a->coeffs[4 * i + 2] << 4)); */
    aux = t[1] >> 6;
    aux = __or_lsl_inplace(aux, t[2], 4);
    r[(int) j] = aux; j += 1;

    /* r[5 * i + 3] = (uint8_t) ((a->coeffs[4 * i + 2] >> 4) | (a->coeffs[4 * i + 3] << 6)); */
    aux = t[2] >> 4;
    aux = __or_lsl_inplace(aux, t[3], 6);
    r[(int) j] = aux; j += 1;
    
    /* r[5 * i + 4] = (uint8_t) (a->coeffs[4 * i + 3] >> 2);                                 */
    aux = t[3] >> 2;
    r[(int) j] = aux; j += 1;
  }
  return r;
}

fn _polyt1_pack(reg ptr u8[POLYT1_PACKEDBYTES] r, reg ptr u32[N] a) -> reg ptr u8[POLYT1_PACKEDBYTES] {
  r = __polyt1_pack(r, a);
  return r;
}	 

/*************************************************
* Name:        JASMIN_MLDSA_polyt1_unpack
*
* Description: Unpack polynomial t1 with 10-bit coefficients.
*              Output coefficients are standard representatives.
*
* Arguments:   - poly *r: pointer to output polynomial
*              - const uint8_t *a: byte array with bit-packed polynomial
**************************************************/
inline fn __polyt1_unpack(reg ptr u32[N] r, reg ptr u8[POLYT1_PACKEDBYTES] a) -> reg ptr u32[N] {
  reg u32 i, j, aux;
  reg u32[5] t;
  inline int k;
  i = 0; j = 0;
  while(i < N)
  {
    for k = 0 to 5 {
      t[k] = (32u) a[(int) j]; j += 1;
    }	
  
    /* r->coeffs[4 * i + 0] = ((a[5 * i + 0] >> 0) | ((uint32_t)a[5 * i + 1] << 8)) & 0x3FF; */
    aux = t[0];
    aux = __or_lsl_inplace(aux, t[1], 8);
    aux = __ubfx(aux, 0, 10);
    r[(int) i] = aux; i += 1;
  
    /* r->coeffs[4 * i + 1] = ((a[5 * i + 1] >> 2) | ((uint32_t)a[5 * i + 2] << 6)) & 0x3FF; */
    aux = t[1] >> 2;
    aux = __or_lsl_inplace(aux, t[2], 6);
    aux = __ubfx(aux, 0, 10);
    r[(int) i] = aux;	i += 1;
  
    /* r->coeffs[4 * i + 2] = ((a[5 * i + 2] >> 4) | ((uint32_t)a[5 * i + 3] << 4)) & 0x3FF; */
    aux = t[2] >> 4;
    aux = __or_lsl_inplace(aux, t[3], 4);
    aux = __ubfx(aux, 0, 10);
    r[(int) i] = aux; i += 1;
  
    /* r->coeffs[4 * i + 3] = ((a[5 * i + 3] >> 6) | ((uint32_t)a[5 * i + 4] << 2)) & 0x3FF; */
    aux = t[3] >> 6;
    aux = __or_lsl_inplace(aux, t[4], 2);
    aux = __ubfx(aux, 0, 10);
    r[(int) i] = aux; i += 1;
    
  }
  return r;
}

fn _polyt1_unpack(reg ptr u32[N] r, reg ptr u8[POLYT1_PACKEDBYTES] a) -> reg ptr u32[N] {
  r = __polyt1_unpack(r, a);
  return r;
}

/*************************************************
* Name:        JASMIN_MLDSA_polyw1_pack
*
* Description: Bit-pack polynomial w1 with coefficients in [0,15] or [0,43].
*              Input coefficients are assumed to be standard representatives.
*
* Arguments:   - uint8_t *r: pointer to output byte array with at least
*                            POLYW1_PACKEDBYTES bytes
*              - const poly *a: pointer to input polynomial
**************************************************/

inline fn __polyw1_pack(reg ptr u8[POLYW1_PACKEDBYTES] r, reg ptr u32[N] a) -> reg ptr u8[POLYW1_PACKEDBYTES] {
  reg u32 i, j, aux;
  reg u32[2] t;
  inline int k;
  i = 0; j = 0;
  while (i < N) {
    for k = 0 to 2 { t[k] = a[(int) i]; i += 1; }
    aux = __or_lsl(t[0], t[1], 4);
    r[(int) j] = aux; j += 1;
  }
  return r;
}

fn _polyw1_pack(reg ptr u8[POLYW1_PACKEDBYTES] r, reg ptr u32[N] a) -> reg ptr u8[POLYW1_PACKEDBYTES] {
  r = __polyw1_pack(r, a);
  return r;
}


/*************************************************
* Name:        JASMIN_MLDSA_polyz_pack
*
* Description: Bit-pack polynomial with coefficients
*              in [-(GAMMA1 - 1), GAMMA1].
*
* Arguments:   - uint8_t *r: pointer to output byte array with at least
*                            POLYZ_PACKEDBYTES bytes
*              - const poly *a: pointer to input polynomial
**************************************************/
inline fn __polyz_pack(reg ptr u8[POLYZ_PACKEDBYTES] r, reg ptr u32[N] a) -> reg ptr u8[POLYZ_PACKEDBYTES] {
  reg u32 i, j, aux, gamma1;
  reg u32[2] t;
  inline int k;

  gamma1 = iGAMMA1;

  i = 0; j = 0;
  while (i < N) {
    for k = 0 to 2 {
       t[k] = a[(int) i]; t[k] = gamma1 - t[k];
       i += 1;
    }

    /* r[5 * i + 0]  = (uint8_t) t[0]; */
    r[(int) j] = t[0]; j += 1;
    
    /* r[5 * i + 1]  = (uint8_t) (t[0] >> 8); */
    aux = t[0] >> 8;
    r[(int) j] = aux; j += 1;
    
    /* r[5 * i + 2]  = (uint8_t) (t[0] >> 16); */
    /* r[5 * i + 2] |= (uint8_t) (t[1] << 4); */

    aux = t[0] >> 16;
    aux = __or_lsl_inplace(aux, t[1], 4);
    r[(int) j] = aux; j += 1;

    /* r[5 * i + 3]  = (uint8_t) (t[1] >> 4); */
    aux = t[1] >> 4;
    r[(int) j] = aux; j += 1;

    /* r[5 * i + 4]  = (uint8_t) (t[1] >> 12); */
    aux = t[1] >> 12;
    r[(int) j] = aux; j += 1;

  }
  return r;
}

fn _polyz_pack(reg ptr u8[POLYZ_PACKEDBYTES] r, reg ptr u32[N] a) -> reg ptr u8[POLYZ_PACKEDBYTES] {
  r = __polyz_pack(r, a);
  return r;
}

/*************************************************
* Name:        JASMIN_MLDSA_polyz_unpack
*
* Description: Unpack polynomial z with coefficients
*              in [-(GAMMA1 - 1), GAMMA1].
*
* Arguments:   - poly *r: pointer to output polynomial
*              - const uint8_t *a: byte array with bit-packed polynomial
**************************************************/

inline fn __polyz_unpack(reg ptr u32[N] r, reg ptr u8[POLYZ_PACKEDBYTES] a) -> reg ptr u32[N] {
  reg u32 i, j, gamma1, aux;
  reg u32[5] t;
  inline int k;
  
  gamma1 = iGAMMA1;
  i = 0; j = 0;
  while(i < N) {
    for k = 0 to 5 { t[k] = (32u) a[(int) j]; j += 1; }
    
    /* r->coeffs[2 * i + 0]  = a[5 * i + 0];  
       r->coeffs[2 * i + 0] |= (uint32_t)a[5 * i + 1] << 8; 
       r->coeffs[2 * i + 0] |= (uint32_t)a[5 * i + 2] << 16; 
       r->coeffs[2 * i + 0] &= 0xFFFFF; */
    aux = __or_lsl(t[0], t[1], 8);
    aux = __or_lsl_inplace(aux, t[2], 16);
    aux = __ubfx(aux, 0, 20);
    aux = gamma1 - aux;
    r[(int) i] = aux; i += 1;
    
    /* r->coeffs[2 * i + 1]  = a[5 * i + 2] >> 4; */
    /* r->coeffs[2 * i + 1] |= (uint32_t)a[5 * i + 3] << 4; */
    /* r->coeffs[2 * i + 1] |= (uint32_t)a[5 * i + 4] << 12; */
    /* r->coeffs[2 * i + 0] &= 0xFFFFF; */  // this look like a lucky bug
    aux = t[2] >> 4;
    aux = __or_lsl_inplace(aux, t[3], 4);
    aux = __or_lsl_inplace(aux, t[4], 12);
    aux = gamma1 - aux;
    r[(int) i] = aux; i += 1;

    // Moved up
    /* r->coeffs[2 * i + 0] = GAMMA1 - r->coeffs[2 * i + 0]; */
    /* r->coeffs[2 * i + 1] = GAMMA1 - r->coeffs[2 * i + 1]; */
  }
  return r;
}

fn _polyz_unpack(reg ptr u32[N] r, reg ptr u8[POLYZ_PACKEDBYTES] a) -> reg ptr u32[N] {
  r = __polyz_unpack(r, a);
  return r;
}

inline fn __extr_rej_sample(
  reg ptr u32[N] a, 
  reg u32 ctr, 
  reg u32 t, 
  reg u32 q
) -> reg ptr u32[N], reg u32 
{
  t = __ubfx(t, 0, 23);
  a, ctr = __rej_sample(a, ctr, t, q);
  return a, ctr;
}

fn _poly_uniform(reg ptr u32[N] a,
                 reg ptr u8[SEEDBYTES] seed,
                 reg u32 nonce
) -> reg ptr u32[N]
{
  stack u32[25*2] state_;
  reg ptr u32[25*2] state;
  stack ptr u32[N] sa;
  sa = a; state = state_;
  state = _stream128_init(state, seed, nonce);
  a = sa;
  
  reg u32 ctr, ctr4, pos, q, t;
  reg u32[3] buf;

  ctr = 0; pos = 0; q = __Q();

  while { ctr4 = ctr + 4; } (ctr4 < N) {
    state, a, pos, ctr, q, buf = _stream128_refill_buffer(state, a, pos, ctr, q);

    a, ctr = __extr_rej_sample(a, ctr, buf[0], q);  // 3 bytes in buf[0]

    t = buf[0] >>u 24;
    t = __or_lsl_inplace(t, buf[1], 8);
    a, ctr = __extr_rej_sample(a, ctr, t, q);

    t = buf[1] >>u 16;
    t = __or_lsl_inplace(t, buf[2], 16);  // 2 byte in buf[1] U 1 bytes in buf[2]
    a, ctr = __extr_rej_sample(a, ctr, t, q);

    t = __ubfx(buf[2], 8, 23);
    a, ctr = __rej_sample(a, ctr, t, q);
  }

  while (ctr < N) {
    state, a, pos, ctr, q, buf = _stream128_refill_buffer(state, a, pos, ctr, q);

    a, ctr = __extr_rej_sample(a, ctr, buf[0], q);  // 3 bytes in buf[0]

    if (ctr < N) {
      t = buf[0] >>u 24;
      t = __or_lsl_inplace(t, buf[1], 8); // 1 byte in buf[0] U 2 bytes in buf[1]
      a, ctr = __extr_rej_sample(a, ctr, t, q);
    }

    if (ctr < N) {
      t = buf[1] >>u 16;
      t = __or_lsl_inplace(t, buf[2], 16); // 2 byte in buf[1] U 1 bytes in buf[2]
      a, ctr = __extr_rej_sample(a, ctr, t, q);
    }

    if (ctr < N) {
      t = __ubfx(buf[2], 8, 23);
      a, ctr = __rej_sample(a, ctr, t, q);
    }
  }
  return a;
} 

/*************************************************
* Name:        JASMIN_MLDSA_poly_uniform_eta
*
* Description: Sample polynomial with uniformly random coefficients
*              in [-ETA,ETA] by performing rejection sampling on the
*              output stream from SHAKE256(seed|nonce) or AES256CTR(seed,nonce).
*
* Arguments:   - poly *a: pointer to output polynomial
*              - const uint8_t seed[]: byte array with seed of length CRHBYTES
*              - uint16_t nonce: 2-byte nonce
**************************************************/

inline fn rej_eta(
  reg ptr u32[N] a, 
  reg u32 ctr, 
  reg u32 t,
  reg u32 n205
) -> reg ptr u32[N], reg u32 
{
  if (t < 15) {
    reg u32 aux;
    aux = t * n205;
    aux = aux >> 10;
    aux = __plus_lsl_inplace(aux, aux, 2);
    aux = t - aux;
    aux = __imm_sub_reg_inplace(2, aux);
    a[(int) ctr] = aux;
    ctr += 1;
  }
  return a, ctr;
}

fn _poly_uniform_eta(reg ptr u32[N] a,
                 reg ptr u8[CRHBYTES] seed,
                 reg u32 nonce
) -> reg ptr u32[N]
{
  stack u32[25*2] state_;
  reg ptr u32[25*2] state;
  stack ptr u32[N] sa;
  sa = a; state = state_;
  state = _stream256_init(state, seed, nonce);
  a = sa;
  
  reg u32 ctr, ctr8, pos, n205, t;
  reg u32 buf;

  ctr = 0; pos = 0; n205 = 205;

  while { ctr8 = ctr + 8; } (ctr8 < N) {

    state, a, pos, ctr, n205, buf = _stream256_refill_buffer(state, a, pos, ctr, n205);
    inline int i;
    for i = 0 to 8 {
      t = __ubfx(buf, 4*i, 4);
      a, ctr = rej_eta(a, ctr, t, n205);
    }     
  }

  while (ctr < N) {
    state, a, pos, ctr, n205, buf = _stream256_refill_buffer(state, a, pos, ctr, n205);
    inline int i;
    for i = 0 to 8 {
      if ((i == 0) || (ctr < N)) {
        t = __ubfx(buf, 4*i, 4);
        a, ctr = rej_eta(a, ctr, t, n205);
      }
    }       
  }
  return a;
} 

param int POLYZ_LAST = POLYZ_PACKEDBYTES / SHAKE256_RATE * SHAKE256_RATE;
param int POLYZ_LAST_SZ = POLYZ_PACKEDBYTES % SHAKE256_RATE;


fn _poly_uniform_gamma1(reg ptr u32[N] a,
                 reg ptr u8[CRHBYTES] seed,
                 reg u32 nonce
) -> reg ptr u32[N]
{
  stack u8[POLYZ_PACKEDBYTES] buf_;
  reg ptr u8[POLYZ_PACKEDBYTES] buf;
  reg ptr u8[SHAKE256_RATE] bufb;
  stack u32[25*2] state_;
  reg ptr u32[25*2] state;
  stack ptr u32[N] sa;
  sa = a; state = state_;
  state = _stream256_init(state, seed, nonce);
  
  inline int k;
  for k = 0 to POLYZ_PACKEDBYTES / SHAKE256_RATE {
    bufb = buf_[SHAKE256_RATE * k : SHAKE256_RATE];
    bufb = _stream256_full_squeeze(bufb, state);
    buf_[SHAKE256_RATE * k : SHAKE256_RATE] = bufb;
    state = _keccakf1600_ref(state);
  }

  reg ptr u8[POLYZ_LAST_SZ] bufc;
  bufc = buf_[ POLYZ_LAST : POLYZ_LAST_SZ];

  reg u32 i, t;
  i = 0;
  while (i < POLYZ_LAST_SZ / 4) {
    t = state[(int) i];
    bufc[u32 (int) i] = t;
    i += 1;
  }
  buf_[  POLYZ_LAST : POLYZ_LAST_SZ] = bufc;

  buf = buf_; a = sa;
  a = _polyz_unpack(a, buf);
  return a;
}
