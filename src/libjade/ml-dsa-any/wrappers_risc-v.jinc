/*************************************************
*
* Description: Provide wrappers around functions that have
*              architecture specific implementations
*
* Version : RISC-V
*
**************************************************/

/* ********************************************************************** */
/* Helper functions to facilitate architecture modularity                 */

inline fn __store_imm(inline int imm) -> reg u32 {
  reg u32 out;
  out = imm;
  return out;
}

inline fn __add_large_imm(reg u32 in, inline int imm) -> reg u32 {
  reg u32 out;
  reg u32 temp;
  //TODO check value of imm for introducing or not a temp register
  temp = imm;
  out = in + temp;
  return out;
}

inline fn __imm_sub_reg_inplace(inline int imm, reg u32 in) -> reg u32 {
    reg u32 temp;
    temp = imm;
    in = temp - in;
    return in;
}

inline fn __imm_sub_reg(inline int imm, reg u32 in) -> reg u32 {
    reg u32 out;
    reg u32 temp;
    temp = imm;
    out = temp - in;
    return out;
}

inline fn __set_if_true(reg u32 in, inline bool b) -> reg u32 {
  reg u32 out;
  if (b) {
    out = in;
  }
  return out;
}

inline fn __set_if_true_plus_imm(reg u32 in, inline int i, inline bool b) -> reg u32 {
  reg u32 out;
  reg u32 temp;
  if (b) {
    temp = in + i;
    out = temp;
  }
  return out;
}

inline fn __cmp_b(reg u32 in_1, reg u32 in_2) -> inline bool {
  inline bool b;
  b = (in_1 <u in_2);
  return b;
}

inline fn __cmp_eq_lt(reg u32 in_1, reg u32 in_2) -> inline bool, inline bool {
  inline bool lt;
  inline bool eq;
  eq = (in_1 == in_2);
  lt = (in_1 <s in_2);
  return eq, lt;
}

inline fn __cmp_gt_le_zero(reg u32 in) -> inline bool, inline bool {
  inline bool gt;
  inline bool le;
  gt = (in >s 0);
  le = (in <=s 0);
  return gt, le;
}

inline fn __ubfx(reg u32 in, inline int pos, inline int width) -> reg u32 {
  in = #SLLI(in, 32 - (pos + width));
  in = #SRLI(in, 32 - width);
  return in;
}

inline fn __smull(reg u32 in_1, reg u32 in_2, reg u32 out_low, reg u32 out_high) -> reg u32, reg u32 {
  out_low = #MUL(in_1,in_2);
  out_high = #MULH(in_1, in_2);
  return out_low, out_high;
}

/**
 Operates the or of the @nb_bits logical left shift on @in_2 with @in_1.
 Writes the result in @in_1.
 Introduces a temporary register.
**/
inline fn __or_lsl_inplace(reg u32 in_1, reg u32 in_2, inline int nb_bits) -> reg u32 {
  reg u32 temp;
  temp = (in_2 << nb_bits);
  in_1 |= temp;
  return in_1;
}

/**
 Operates the or of the @nb_bits logical left shift on @in_2 with @in_1.
**/
inline fn __or_lsl(reg u32 in_1, reg u32 in_2, inline int nb_bits) -> reg u32 {
  reg u32 out;
  out = (in_2 << nb_bits);
  out |= in_1;
  return out;
}

/**
 Operates the and of the @nb_bits arithmetic right shift on @in_2 with @in_1.
**/
inline fn __and_asr(reg u32 in_1, reg u32 in_2, inline int nb_bits) -> reg u32 {
  reg u32 out;
  out = (in_2 >>s nb_bits);
  out &= in_1;
  return out;
}

/**
 Operates the addition of the @nb_bits logical left shift on @in_2 with @in_1.
 Writes the result in @in_1.
 Introduces a temporary register.
**/
inline fn __plus_lsl_inplace(reg u32 in_1, reg u32 in_2, inline int nb_bits) -> reg u32 {
  reg u32 temp;
  temp = (in_2 << nb_bits);
  in_1 += temp;
  return in_1;
}

/**
 Operates the addition of the @nb_bits logical left shift on @in_2 with @in_1.
 Writes the result in @out.
**/
inline fn __plus_lsl(reg u32 in_1, reg u32 in_2, inline int nb_bits) -> reg u32 {
  reg u32 out;
  out = (in_2 << nb_bits);
  out = in_1 + out;
  return out;
}

/**
 Operates the subtraction of @in_1 with the @nb_bits logical left shift on @in_2.
 Writes the result in @out.
**/
inline fn __sub_lsl(reg u32 in_1, reg u32 in_2, inline int nb_bits) -> reg u32 {
  reg u32 out;
  reg u32 temp;
  temp = in_2 << nb_bits;
  out = in_1 - temp;
  return out;
}

/**
 Operates the subtraction of the @nb_bits logical left shift on @in_2 with @in_1.
 Writes the result in @out.
**/
inline fn __rsub_lsl(reg u32 in_1, inline int in_2, inline int nb_bits) -> reg u32 {
  reg u32 out;
  reg u32 temp;
  temp = (in_2 << nb_bits);
  out = temp - in_1;
  return out;
}

/**
 Operates the subtraction of the @nb_bits logical left shift on @in_2 with @in_1.
 Writes the result in @in_2.
**/
inline fn __rsb_lsl_inplace(reg u32 in_1, reg u32 in_2, inline int nb_bits) -> reg u32 {
  in_2 = in_2 << nb_bits;
  in_2 = in_2 - in_1;
  return in_2;
}

/**
 Operates the subtraction of the @nb_bits logical left shift on @in_2 with @in_1.
**/
inline fn __rsb_lsl(reg u32 in_1, reg u32 in_2, inline int nb_bits) -> reg u32 {
  reg u32 out;
  in_2 = in_2 << nb_bits;
  out = in_2 - in_1;
  return out;
}
