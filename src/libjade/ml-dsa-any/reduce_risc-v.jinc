/*************************************************
* Name:        JASMIN_MLDSA87_montgomery_reduce
*
* Description: For finite field element a with -2^{31}Q <= a <= Q*2^31,
*              compute r \equiv a*2^{-32} (mod Q) such that -Q < r < Q.
*
* Arguments:   - int64_t: finite field element a
*
* Returns r.
**************************************************/

inline fn __montgomery_reduce_8380417(reg u32 a_low a_high) -> reg u32
{
  reg u32 a_low_x7 a_low_x7169 a_low_x58728449;
  reg u32 res;
  reg u32 q minus_q;
  reg u32 t0;
  minus_q = MINUS_Q;
  q = iQ;

  /// The following two attempts to construct a_low_x58728449 are equivalent
  // #1
  t0 = a_low << 3;
  a_low_x7 = t0 - a_low;
  t0 = a_low_x7 << 10;
  a_low_x7169 = a_low + t0;
  t0 = a_low_x7169 << 13;
  a_low_x58728449 = a_low + t0;

  // #2
  // qinv = 58728449;
  // a_low_x58728449 = #MUL(a_low, qinv);

  reg u32 txQ_high, txQ_low, tp_high, tp_low;
  txQ_low = #MUL(a_low_x58728449, q);
  txQ_high = #MULH(a_low_x58728449, q);

  tp_low = a_low - txQ_low;
  tp_high = a_high - txQ_high;
  if tp_low >32u a_low { tp_high = tp_high + 1; } // carry

  res = tp_high;
  return res;
}
